package view;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private OperationPanel operationPanel;
	private TextPanel textPanel;

	public MainFrame() {
		super("Polynomial Operations");
		setLayout(new BorderLayout());

		textPanel = new TextPanel();
		operationPanel = new OperationPanel();

		add(textPanel, BorderLayout.CENTER);
		add(operationPanel, BorderLayout.WEST);
		
		operationPanel.setStringListener(new StringListener() {
			public void textEmitted(String text) {
				textPanel.appendText(text);
			}
		});
		
		
		setMinimumSize(new Dimension(500, 400));
		setSize(600, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
}