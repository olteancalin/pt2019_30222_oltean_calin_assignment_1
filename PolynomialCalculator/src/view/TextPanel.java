package view;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class TextPanel extends JPanel {
	JTextArea textArea;

	public TextPanel() {
		textArea = new JTextArea();
		textArea.setFont(textArea.getFont().deriveFont(20f));
		setLayout(new BorderLayout());
		add(new JScrollPane(textArea), BorderLayout.CENTER);
	}

	public void appendText(String text) {
		textArea.append(text);
		textArea.append("\n");
	}
}
