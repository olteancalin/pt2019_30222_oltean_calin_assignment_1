package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import model.MathCommand;
import model.MathOperation;
import model.Monomial;
import model.Polynomial;
import model.StrictInputVerifier;

@SuppressWarnings("serial")
public class OperationPanel extends JPanel {
	private JLabel firstPolynomialLabel;
	private JLabel secondPolynomialLabel;
	JTextField firstPolynomialField;
	JTextField secondPolynomialField;
	private JComboBox<MathCommand> operationCombo;
	private JButton okBtn;
	private boolean isValidFirst = false;
	private boolean isValidSecond = false;
	private StringListener textListener;
	private Polynomial firstPolynomial;
	private Polynomial secondPolynomial;

	public OperationPanel() {
		Dimension dim = getPreferredSize();
		dim.width = 250;
		setPreferredSize(dim);
		
		firstPolynomialLabel = new JLabel("Polynomial1: ");
		secondPolynomialLabel = new JLabel("Polynomial2: ");

		firstPolynomialField = new JTextField(10);
		firstPolynomialField.setText("1x^3-2x^1+1x^0");
		// firstPolynomialField.setText("2x^5-7x^2+9x^0");

		secondPolynomialField = new JTextField(10);
		secondPolynomialField.setText("1x^3+2x^2");
		// secondPolynomialField.setText("2x^5-3x^1");

		operationCombo = new JComboBox<>();

		operationCombo.setModel(new DefaultComboBoxModel<>(MathCommand.values()));
		operationCombo.setSelectedIndex(0);
		operationCombo.setEditable(false);
		okBtn = new JButton("OK");
		okBtn.setEnabled(false);
		firstPolynomialField.setInputVerifier(new InputVerifier() {
			public boolean verify(JComponent input) {
				if (input instanceof JTextField) {
					StrictInputVerifier verifier = new StrictInputVerifier();
					isValidFirst = verifier.formatVerifier(firstPolynomialField.getText());
					System.out.println(isValidFirst);
					if (isValidFirst && isValidSecond) {
						okBtn.setEnabled(true);
					} else {
						okBtn.setEnabled(false);
					}
					firstPolynomial = new Polynomial(verifier);
					for (Monomial nam : firstPolynomial.getPolynomialList()) {
						System.out.println(nam.getCoefficient() + " " + nam.getExponent() + " from verifier");
					}
				}
				return true;
			}
		});

		secondPolynomialField.setInputVerifier(new InputVerifier() {
			public boolean verify(JComponent input) {
				if (input instanceof JTextField) {
					StrictInputVerifier verifier = new StrictInputVerifier();
					isValidSecond = verifier.formatVerifier(secondPolynomialField.getText());
					System.out.println(isValidSecond);
					if (isValidFirst && isValidSecond) {
						okBtn.setEnabled(true);
					} else {
						okBtn.setEnabled(false);
					}
					secondPolynomial = new Polynomial(verifier);
					for (Monomial nam : secondPolynomial.getPolynomialList()) {
						System.out.println(nam.getCoefficient() + " " + nam.getExponent() + " from verifier");
					}
				}
				return true;
			}
		});

		okBtn.addActionListener(e -> {
			MathOperation operation = new MathOperation(firstPolynomial, secondPolynomial);
			textListener.textEmitted(operation.selectOperation(operationCombo.getSelectedItem().toString()));
		});

		Border innerBorder = BorderFactory.createTitledBorder("Add Operands");
		Border outerBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
		setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));

		layoutComponents();
	}

	public final void layoutComponents() {

		setLayout(new GridBagLayout());

		GridBagConstraints gc = new GridBagConstraints();

		// First Polynomial
		gc.gridy = 0;

		gc.weightx = 1;
		gc.weighty = 0.1;

		gc.gridx = 0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(firstPolynomialLabel, gc);

		gc.gridx = 1;
		gc.gridy = 0;
		gc.insets = new Insets(0, 0, 0, 0);
		gc.anchor = GridBagConstraints.LINE_START;
		add(firstPolynomialField, gc);
		// Second Polynomial
		gc.gridy++;

		gc.weightx = 1;
		gc.weighty = 0.1;

		gc.gridx = 0;
		gc.insets = new Insets(0, 0, 0, 5);
		gc.anchor = GridBagConstraints.LINE_END;
		add(secondPolynomialLabel, gc);

		gc.gridx = 1;
		gc.insets = new Insets(0, 0, 0, 0);
		gc.anchor = GridBagConstraints.LINE_START;
		add(secondPolynomialField, gc);
		// Enum Combo
		gc.gridy++;

		gc.weightx = 1;
		gc.weighty = 0.2;

		gc.gridx = 0;
		gc.insets = new Insets(0, 0, 0, 5);
		gc.anchor = GridBagConstraints.FIRST_LINE_END;
		add(new JLabel("Operation: "), gc);

		gc.gridx = 1;
		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(operationCombo, gc);
		// Ok Button
		gc.gridy++;

		gc.weightx = 1;
		gc.weighty = 2.0;

		gc.gridx = 1;
		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(okBtn, gc);
	}

	public void setStringListener(StringListener listener) {
		this.textListener = listener;
	}

}
