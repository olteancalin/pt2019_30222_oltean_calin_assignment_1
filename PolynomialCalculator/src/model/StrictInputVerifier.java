package model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StrictInputVerifier {
	private List<Monomial> polynomialList;

	public StrictInputVerifier() {
		polynomialList = new ArrayList<Monomial>();
	}

	public List<Monomial> getVerifierPolynomial() {
		return polynomialList;
	}

	public boolean formatVerifier(String inputString) {
		try {
			if (inputString.length() == 0) {
				throw new Exception();
			}
			Pattern p = Pattern
			        .compile("([+-]?\\d*\\.?\\d*)[xX]\\^(-?\\d+\\b)|([+-]?\\d*\\.?\\d*)[xX]|([+-]?\\d*\\.?\\d*)");
			Matcher m = p.matcher(inputString);
			while (m.find()) {
				if (m.group(1) != null && m.group(2) != null) {
					polynomialList.add(new Monomial(Float.parseFloat(m.group(1)), Integer.parseInt(m.group(2))));

				}
				if (m.group(3) != null && m.group(3).length() != 0) {
					polynomialList.add(new Monomial(Float.parseFloat(m.group(3)), 1));
					break;
				}
				if (m.group(4) != null && m.group(4).length() != 0) {
					polynomialList.add(new Monomial(Float.parseFloat(m.group(4)), 0));

				}
				if (m.group().length() == 0 && m.hitEnd() == false) {
					throw new Exception();
				}
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
