package model;

public class MathOperation {

	private Polynomial firstPolynomial;
	private Polynomial secondPolynomial;

	public MathOperation(Polynomial firstPolynomial, Polynomial seconPolynomial) {
		this.firstPolynomial = firstPolynomial;
		this.secondPolynomial = seconPolynomial;
	}

	public final String selectOperation(String operation) {
		Polynomial polynomialResult;
		Polynomial polynomialRest;
		switch (operation) {
		case "ADD":
			polynomialResult = new Polynomial(secondPolynomial);
			polynomialResult.polynomialAdd(firstPolynomial);
			return polynomialResult.getPolynomialString();
		case "SUBTRACT":
			polynomialResult = new Polynomial(secondPolynomial);
			polynomialResult.polynomialSubtract(firstPolynomial);
			return polynomialResult.getPolynomialString();

		case "MULTIPLY":
			polynomialResult = new Polynomial();
			polynomialResult.polynomialMultiply(firstPolynomial, secondPolynomial);
			return polynomialResult.getPolynomialString();

		case "DIVIDE":
			polynomialResult = new Polynomial();
			polynomialRest = new Polynomial();
			polynomialResult.polynomialDivide(firstPolynomial, secondPolynomial, polynomialRest);
			return polynomialResult.getPolynomialString() + " Rest: " + polynomialRest.getPolynomialString();

		case "INTEGRATION":
			polynomialResult = new Polynomial();
			polynomialResult.polynomialIntegration(firstPolynomial);
			return polynomialResult.getPolynomialString();

		case "DERIVATION":
			polynomialResult = new Polynomial();
			polynomialResult.polynomialDerivation(firstPolynomial);
			return polynomialResult.getPolynomialString();

		default:
			return "Default";
		}
	}
}
