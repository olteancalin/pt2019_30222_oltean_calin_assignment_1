package model;

public class Monomial {

	private float coefficient;
	private int exponent;
	
	public Monomial(float coefficient,int exponent) {
		this.coefficient=coefficient;
		this.exponent=exponent;
	}
	
	public void setCoefficient(float coefficient) {
		this.coefficient = coefficient;
	}

	public float getCoefficient() {
		return coefficient;
	}

	public void setExponent(int exponent) {
		this.exponent = exponent;
	}

	public int getExponent() {
		return exponent;
	}

}
