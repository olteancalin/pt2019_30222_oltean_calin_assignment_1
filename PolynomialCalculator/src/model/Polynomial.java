package model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Polynomial {
	private List<Monomial> polynomialList;

	public Polynomial() {
		polynomialList = new ArrayList<Monomial>();
	}

	public Polynomial(Polynomial poly) {
		this();
		for (Monomial nam : poly.polynomialList) {
			this.polynomialList.add(new Monomial(nam.getCoefficient(), nam.getExponent()));
		}
	}

	public Polynomial(StrictInputVerifier poly) {
		this();
		for (Monomial nam : poly.getVerifierPolynomial()) {
			this.polynomialList.add(new Monomial(nam.getCoefficient(), nam.getExponent()));
		}
	}

	public List<Monomial> getPolynomialList() {
		return polynomialList;
	}

	public void polynomialAdd(Polynomial poly1) {
		for (Monomial pl1 : poly1.polynomialList) {
			boolean flag = false;
			for (Monomial res : this.polynomialList) {
				if (pl1.getExponent() == res.getExponent()) {
					res.setCoefficient(pl1.getCoefficient() + res.getCoefficient());
					flag = true;
				}
			}
			if (!flag) {
				this.polynomialList.add(pl1);
			}
		}
	}

	public final void polynomialSubtract(Polynomial poly1) {
		for (Monomial invertResult : this.polynomialList) {
			invertResult.setCoefficient(-invertResult.getCoefficient());
		}

		for (Monomial pl1 : poly1.polynomialList) {
			boolean flag = false;
			for (Monomial res : this.polynomialList) {
				if (pl1.getExponent() == res.getExponent()) {
					res.setCoefficient(pl1.getCoefficient() + res.getCoefficient());
					flag = true;
				}
			}
			if (!flag) {
				this.polynomialList.add(new Monomial(pl1.getCoefficient(), pl1.getExponent()));
			}
		}
	}

	public final void polynomialMultiply(Polynomial poly1, Polynomial poly2) {
		for (Monomial pl1 : poly1.polynomialList) {
			Polynomial temp = new Polynomial();
			for (Monomial pl2 : poly2.polynomialList) {
				temp.polynomialList.add(new Monomial(pl1.getCoefficient() * pl2.getCoefficient(),
				        pl1.getExponent() + pl2.getExponent()));
			}
			this.polynomialAdd(temp);
		}
	}

	public final void polynomialDivide(Polynomial poly1, Polynomial poly2, Polynomial polynomialRest) {
		Polynomial tempPoly1 = new Polynomial(poly1);
		boolean flag = tempPoly1.polynomialList.get(0).getExponent() >= poly2.polynomialList.get(0).getExponent();
		while (flag) {
			Polynomial temp = new Polynomial();
			this.polynomialList.add(new Monomial(
			        tempPoly1.polynomialList.get(0).getCoefficient() / poly2.polynomialList.get(0).getCoefficient(),
			        tempPoly1.polynomialList.get(0).getExponent() - poly2.polynomialList.get(0).getExponent()));
			Polynomial adv = new Polynomial();
			adv.polynomialList.add(this.polynomialList.get(polynomialList.size() - 1));
			temp.polynomialMultiply(adv, poly2);
			temp.polynomialSubtract(tempPoly1);
			temp.getPolynomialList().removeIf(px -> px.getCoefficient() == 0);
			temp.polynomialSort();
			tempPoly1 = new Polynomial(temp);
			if (tempPoly1.polynomialList.get(0).getExponent() < poly2.polynomialList.get(0).getExponent()) {
				for (Monomial rest : tempPoly1.polynomialList) {
					polynomialRest.polynomialList.add(new Monomial(rest.getCoefficient(), rest.getExponent()));
				}
				break;
			}
		}
	}

	public final void polynomialIntegration(Polynomial poly1) {
		for (Monomial pl1 : poly1.polynomialList) {
			if (pl1.getExponent() == 0) {
				this.polynomialList.add(new Monomial(pl1.getCoefficient(), pl1.getExponent() + 1));
			} else {
				this.polynomialList
				        .add(new Monomial(pl1.getCoefficient() / (pl1.getExponent() + 1), pl1.getExponent() + 1));
			}
		}
	}

	public void polynomialDerivation(Polynomial poly1) {
		for (Monomial pl1 : poly1.polynomialList) {
			if (pl1.getExponent() - 1 > -1) {
				this.polynomialList.add(new Monomial(pl1.getCoefficient() * pl1.getExponent(), pl1.getExponent() - 1));
			}
		}
	}

	public void polynomialSort() {
		Comparator<Monomial> powerComparator = (c1, c2) -> (int) (c2.getExponent() - c1.getExponent());
		this.getPolynomialList().sort(powerComparator);
	}

	public String getPolynomialString() {
		StringBuilder polynomialOuput = new StringBuilder();
		this.getPolynomialList().removeIf(px -> px.getCoefficient() == 0);
		if (this.getPolynomialList().isEmpty()) {
			polynomialOuput.append("0");
			return polynomialOuput.toString();
		}
		this.polynomialSort();
		for (Monomial nam : this.getPolynomialList()) {
			if (nam.getCoefficient() > 0) {
				if (this.getPolynomialList().get(0).getExponent() == nam.getExponent()) {
					polynomialOuput.append(nam.getCoefficient());
				}else {
					polynomialOuput.append("+");
					polynomialOuput.append(nam.getCoefficient());
				}
			}else {
				polynomialOuput.append(nam.getCoefficient());
			}
			polynomialOuput.append("x^");
			polynomialOuput.append(nam.getExponent());
		}
		return polynomialOuput.toString();
	}

}
