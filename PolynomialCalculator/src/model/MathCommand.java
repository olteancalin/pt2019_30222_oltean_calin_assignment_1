package model;

public enum MathCommand {
	ADD, SUBTRACT, MULTIPLY, DIVIDE, INTEGRATION, DERIVATION
}
