package test;

import org.junit.jupiter.api.Test;

import org.junit.Assert;

import model.Monomial;
import model.Polynomial;
import model.StrictInputVerifier;
class PolynomialTest {
	
	@Test
	void polynomialSortTest() {
		Polynomial polynomialTest = new Polynomial();
		polynomialTest.getPolynomialList().add(new Monomial(1,3));
		polynomialTest.getPolynomialList().add(new Monomial(2,5));
		polynomialTest.getPolynomialList().add(new Monomial(3,8));
		polynomialTest.getPolynomialList().add(new Monomial(4,9));
		polynomialTest.polynomialSort();
		Assert.assertEquals("4.0x^9+3.0x^8+2.0x^5+1.0x^3",polynomialTest.getPolynomialString());
	}
	
	@Test
	void formatVerifier() {
		StrictInputVerifier verifier = new StrictInputVerifier();
		Assert.assertEquals(true,verifier.formatVerifier("1x^3-2x^1+1x^0"));
	}
	
	@Test
	void addTest() {
		StrictInputVerifier verifier = new StrictInputVerifier();
		StrictInputVerifier verifier2 = new StrictInputVerifier();
		StrictInputVerifier verifier3 = new StrictInputVerifier();

		verifier.formatVerifier("1x^3-2x^1+1x^0");
		Polynomial firstPolynomial = new Polynomial(verifier);
		verifier2.formatVerifier("1x^3+2x^2");
		Polynomial polynomialResult = new Polynomial(verifier2);
		polynomialResult.polynomialAdd(firstPolynomial);
		polynomialResult.polynomialSort();
		
		verifier3.formatVerifier("2.0x^3+2.0x^2-2.0x^1+1.0x^0");
		Polynomial polynomialTesting = new Polynomial(verifier3);
		polynomialTesting.polynomialSort();
		
		Assert.assertEquals(polynomialTesting.getPolynomialString(), polynomialResult.getPolynomialString());

	}
	
	@Test
	void subTest() {
		StrictInputVerifier verifier = new StrictInputVerifier();
		StrictInputVerifier verifier2 = new StrictInputVerifier();
		StrictInputVerifier verifier3 = new StrictInputVerifier();
	
		verifier.formatVerifier("1x^3-2x^1+1x^0");
		Polynomial firstPolynomial = new Polynomial(verifier);
		verifier2.formatVerifier("1x^3+2x^2");
		Polynomial polynomialResult = new Polynomial(verifier2);
		polynomialResult.polynomialSubtract(firstPolynomial);
		polynomialResult.polynomialSort();
		
		verifier3.formatVerifier("-2.0x^2-2.0x^1+1.0x^0");
		Polynomial polynomialTesting = new Polynomial(verifier3);
		polynomialTesting.polynomialSort();
		
		Assert.assertEquals(polynomialTesting.getPolynomialString(), polynomialResult.getPolynomialString());

	}
	
	@Test
	void multTest() {
		StrictInputVerifier verifier = new StrictInputVerifier();
		StrictInputVerifier verifier2 = new StrictInputVerifier();
		StrictInputVerifier verifier3 = new StrictInputVerifier();
		
		verifier.formatVerifier("1x^3-2x^1+1x^0");
		Polynomial firstPolynomial = new Polynomial(verifier);
		verifier2.formatVerifier("1x^3+2x^2");
		Polynomial secondPolynomial = new Polynomial(verifier2);
		Polynomial polynomialResult = new Polynomial();
		polynomialResult.polynomialMultiply(firstPolynomial,secondPolynomial);
		polynomialResult.polynomialSort();
		
		verifier3.formatVerifier("1.0x^6+2.0x^5-2.0x^4-3.0x^3+2.0x^2");
		Polynomial polynomialTesting = new Polynomial(verifier3);
		polynomialTesting.polynomialSort();
		
		Assert.assertEquals(polynomialTesting.getPolynomialString(), polynomialResult.getPolynomialString());

	}
	
	@Test
	void divTest() {
		StrictInputVerifier verifier = new StrictInputVerifier();
		StrictInputVerifier verifier2 = new StrictInputVerifier();
		StrictInputVerifier verifier3 = new StrictInputVerifier();
		StrictInputVerifier verifier4 = new StrictInputVerifier();
		
		verifier.formatVerifier("1x^3-2x^1+1x^0");
		Polynomial firstPolynomial = new Polynomial(verifier);
		verifier2.formatVerifier("1x^3+2x^2");
		Polynomial secondPolynomial = new Polynomial(verifier2);
		Polynomial polynomialResult = new Polynomial();
		Polynomial polynomialRest = new Polynomial();
		
		polynomialResult.polynomialDivide(firstPolynomial,secondPolynomial,polynomialRest);
		polynomialResult.polynomialSort();
		
		verifier3.formatVerifier("1.0x^0");
		Polynomial polynomialTesting = new Polynomial(verifier3);
		polynomialTesting.polynomialSort();
		
		verifier4.formatVerifier("-2.0x^2-2.0x^1+1.0x^0");
		Polynomial polynomialRestTest = new Polynomial(verifier4);
		polynomialRestTest.polynomialSort();
		
		Assert.assertEquals(polynomialTesting.getPolynomialString(), polynomialResult.getPolynomialString());
		Assert.assertEquals(polynomialRestTest.getPolynomialString(), polynomialRest.getPolynomialString());
	}
	
	@Test
	void integrationTest() {
		StrictInputVerifier verifier = new StrictInputVerifier();
		StrictInputVerifier verifier3 = new StrictInputVerifier();
	
		verifier.formatVerifier("1x^3-2x^1+1x^0");
		Polynomial firstPolynomial = new Polynomial(verifier);
		Polynomial polynomialResult = new Polynomial();
		polynomialResult.polynomialIntegration(firstPolynomial);
		polynomialResult.polynomialSort();
		
		verifier3.formatVerifier("0.25x^4-1.0x^2+1.0x^1");
		Polynomial polynomialTesting = new Polynomial(verifier3);
		polynomialTesting.polynomialSort();
		
		Assert.assertEquals(polynomialTesting.getPolynomialString(), polynomialResult.getPolynomialString());

	}
	
	@Test
	void derivationTest() {
		StrictInputVerifier verifier = new StrictInputVerifier();
		StrictInputVerifier verifier3 = new StrictInputVerifier();
	
		verifier.formatVerifier("1x^3-2x^1+1x^0");
		Polynomial firstPolynomial = new Polynomial(verifier);
		Polynomial polynomialResult = new Polynomial();
		polynomialResult.polynomialDerivation(firstPolynomial);
		polynomialResult.polynomialSort();
		
		verifier3.formatVerifier("3.0x^2-2.0x^0");
		Polynomial polynomialTesting = new Polynomial(verifier3);
		polynomialTesting.polynomialSort();
		
		Assert.assertEquals(polynomialTesting.getPolynomialString(), polynomialResult.getPolynomialString());

	}
}
